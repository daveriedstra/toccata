#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# ensure we're in the correct directory
cd $(dirname $0)

# change this to use vanilla or some other binary if you like
PD_PROG="pd-l2ork"

# set cleanup
cleanup() {
    echo "killing jackd and pd"
    killall $PD_PROG
    sleep 1
    killall jackd
    exit 0
}
trap 'cleanup' SIGINT


# performance hacks
mount -o remount,size=128M /dev/shm
for cpu in /sys/devices/system/cpu/cpu[0-9]*; do
	echo -n performance > $cpu/cpufreq/scaling_governor;
done


#start jackd
export DISPLAY=:0
export `su pi -c "dbus-launch | grep ADDRESS"`
export `su pi -c "dbus-launch | grep PID"`

# 3 periods of 256 samples at 44.1kHz = 17.4 ms latency
su $SUDO_USER -c "jackd -R -P70 -p16 -t2000 -dalsa -P -n 3 -p 256 -r 44100 -s -S &> /dev/null &"

# give jackd some time to start
sleep 3

if pgrep "jackd" >/dev/null 2>&1 ; then
    echo "jackd started..."
else
    echo "Could not start jackd!"
    exit -1
fi


#start headless pd
su $SUDO_USER -c "$PD_PROG -nogui -rt -jack -audioindev 0 -audiooutdev 2 -send 'dsp 1;' -open ./pi.pd > /dev/null 2>&1 &"

# give pd some time to start
sleep 3

if pgrep "$PD_PROG" >/dev/null 2>&1 ; then
    echo "$PD_PROG started..."
else
    echo "Could not start $PD_PROG!"
    exit -1
fi

