#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "Usage: ./leader.sh [start|stop|shutdown]"
	echo "This script should be run on the main controller to start or stop all nodes."
	exit 0
fi

SCRIPT_NAME=""
VERB_NAME=""
DIR_NAME="toccata"
EXIT_CODE=0

do_verb() {
	for i in $(seq 1 6)
	do
		echo "$VERB_NAME node$i"
		ssh pi@node$i.local nohup sudo $SCRIPT_NAME
		# accumulate non-successful exits
		EXIT_CODE=$(( $EXIT_CODE | $? ))
	done

	if [ $EXIT_CODE -eq 0 ]
	then
		echo "completed successfully"
	else
		echo "possible error, review output"
	fi
}

if [ "$1" = "start" ]
then
	SCRIPT_NAME="$DIR_NAME/start.sh"
	VERB_NAME="starting"
elif [ "$1" = "stop" ]
then
	SCRIPT_NAME="$DIR_NAME/stop.sh"
	VERB_NAME="stopping"
elif [ "$1" = "shutdown" ]
then
	SCRIPT_NAME="shutdown now"
	VERB_NAME="shutting down"
else
	echo "Invalid option"
	exit 1
fi

do_verb

exit $EXIT_CODE
