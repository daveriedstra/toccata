Toccata
====

Apocryphal Production's Toccata for Hamilton Fringe Frost Bites 2019\
Site-specific to the sanctuary of St. Paul's Presbyterian Church, Hamilton, Ontario\
[More information](https://daveriedstra.com/works/toccata.html)

Kris VanSoelen, direction\
Geneviève Trottier, performance\
Dave Riedstra, sound

# Hardware requirements
* Six Raspberry Pis (3B used, others might work)
* Six HiFiBerry MiniAmp (one for each Pi)
* 12 transducers (A mix of Dayton Audio DAEX19CT-4 and DAEX25CT-4 used)
* Batteries or power supply for Pis
* One "leader" PC running linux
* Korg NanoKontrol2 (or similar midi controller)
* Dedicated WiFi

# Configuration
The six Raspberry Pis, each with two transducer outputs, are distributed throughout the venue in the following pattern:
* 1 in chancel on box behind organ console
* 4 in balcony pew book holders (2 on right, 2 on left), such that transducers spread along the length of the pew
* 1 in balcony rear pew with transducers spread to extreme left and right
Transducers are in non-permanent install, weighted with books or plasticine as available. Pew transducers and Pis are covered with wood strips stained to match colour of pew.

## Individual Pi system & software
Each pi has the same system configuration, with the exception of hostname: these should be numbered node1 - node6 to match the scripts in this repo. Each pi should have the following software installed:
* Raspbian OS
* pd-l2ork
* jackd
* [dried-utils](https://gitlab.com/daveriedstra/dried-utils) in a folder named `dried` in the pd-l2ork search path
* inst in the pd-l2ork search path _(nb: not yet open-sourced, coming soon)_
* clone of this repo in `~`

## Leader system
The Pis should be accessible by SSH from the "leader" station on the same network. The leader station is used with the MikroKontrol to control the sounds throughout the show. The leader station needs the following software:
* Some distro of linux (Ubuntu 18.04 used)
* pd-l2ork
* qjacktl or some other way of routing midi to pd-l2ork
The leader station exclusively processes control data, no audio, and can be quite a light system.

# Usage
## Pre-show
* start wifi network if not already started
* Place all Pis and power on
* Ping all Pis from leader: `for i in $(seq 1 6); do ping -c1 node$i.local; done`
* `./leader.sh start` to start pd and jackd on pis
* Open `leader.pd` in pd-l2ork
* Route midi controller data to pd
* initialize values by moving through click / pitch modes on each node and tweaking all knobs and sliders

## Post-show
* `./leader stop` to stop pd and jackd
* `./leader shutdown` to shutdown all Pis
